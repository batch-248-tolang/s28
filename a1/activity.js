
// STEP 2
db.rooms.insert({
    name: "single",
    accomodates: 2,
    price: 1000,
    description: "A simple room with all the basic necessities",
    roomsAvailable: 10,
    isAvailable: false
});

// STEP 3&4
db.rooms.insertMany([
    {
    name: "double",
    accomodates: 3,
    price: 2000,
    description: "A room fit for a small family going on a vacation",
    roomsAvailable: 5,
    isAvailable: false
    },
    {
    name: "queen",
    accomodates: 4,
    price: 4000,
    description: "A room with a queen sized bed perfect for a simple getaway",
    roomsAvailable: 15,
    isAvailable: false
    }

]);

// STEP 5
db.rooms.find({name:"double"});

// STEP 6
db.rooms.updateOne(
    { name: "queen" },
    {
        $set : {
            roomsAvailable: 0
        }
    }
);

// STEP 7
db.rooms.deleteMany({
     roomsAvailable: 0
});