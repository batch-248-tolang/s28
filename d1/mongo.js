//CRUD Operations
/*
	CRUD Operations are the heart of any backend application

*/

//Insert Documents (Create)

//Inserting one document

	//Syntax
		//db.collectionName.insertOne({object});
		//db.collectionName.insert({object});
	
	//JavaScript syntax comparison
		//object.object.method({object});

	db.users.insert({
	    firstName:"Jane",
	    lastName:"Doe",
	    age: 21,
	    contact: {
	        phone: "87654321",
	        email: "janedoe@gmail.com"
	    },
	    courses: ["CSS","Javascript","Python"],
	    department: "none"
	});

	//Insert Many

	// Syntax
		// db.collectionName.insertMany([{objectA},{objectB}]);
	
	db.users.insertMany([
		{
			firstName: "Stephen",
			lastName:"Hawking",
			age: 76,
			contact:{
				phone:"87654321",
				email:"stephenhawking@gmail.com"
			},
			courses:["Python","React","PHP"],
			department:"none"
		},
		{
			firstName: "Neil",
			lastName:"Armstrong",
			age: 82,
			contact:{
				phone:"87654321",
				email:"neilarmstrong@gmail.com"
			},
			courses:["React","Laravel","Sass"],
			department:"none"
		}
	]);


	//Finding Documents (Read/Retrieve)

	//Find
	/*
		-if multiple documents match the criteria for finding a document only the FIRST document that matches the search term will be returned
		-this is also based from the order that documents are stored in a collection
		-if the document is not found, the terminal will respond with a blank line

		Syntax:
		- db.collectionName.find();
		- db.collectionName.find({field:value});

	*/

	//Finding a single document
	//leaving the search criteria empty will retrieve ALL documents

	db.users.find();

	db.users.find({firstName:"Stephen"});


	//Finding documents with multiple parameters

	/*
		Syntax
			db.collectionName.find({fieldA: valueA, fieldB: valueB});

	*/

	// miniA 3min
		//find a document with lastName (Armstrong) age (82)
		//execute in Robo3T


	db.users.find({lastName:"Armstrong",age:82});

	//Updating documents

	//update a single document

	//*******create a document that we will update

	db.users.insert({

		firstName: "Test",
		lastName:"Test",
		age:0,
		contact:{
			phone: "00000000",
			email: "test@gmail.com"
		},
		courses: [],
		department: "none"

	});

	/*
		Just like the "find" method, methods that only manipulate a single document will only update the FIRST document that matches the search criteria

		Syntax:

		db.collectioName.updateOne({criteria},{$set: {field:value}})

	*/

	db.users.updateOne(
		{ firstName: "Test" },
		{
			$set : {
				firstName: "Bill",
				lastName: "Gates",
				age: 65,
				contact:{
					phone: "12345678",
					email:"bill@gmail.com"
				},
				courses:["PHP","Laravel","HTML"],
				department:"Operations",
				status:"active"
			}
		}
	);

	//Updating multiple documents
	/*
		Syntax:
		db.collectionName.updateMany({criteria},{$set:{field:value}});

	*/

	db.users.updateMany(
		{department:"none"},
		{
			$set:{department:"HR"}
		}
	);

	//replaceone

	/*
		Can be used if replacing the whole document is necessary
		Syntax:
			db.collectionName.replaceOne({criteria},{field:value});

	*/

	db.users.replaceOne(

		{firstName:"Bill"},
		{
			firstName:"Bill",
			lastName:"Gates",
			age:65,
			contact:{
				phone:"12345678",
				email:"bill@gmail.com"
			},
			courses:["PHP","Laravel","HTML"],
			department:"Operations"
		}

	)

	//deleting documents (Delete)

	//creating a document to delete

	db.users.insert({
		firstName:"test"
	});

	//delete a single document
	/*
		Syntax:
		db.collectionName.deleteOne({criteria});

	*/

	db.users.deleteOne({
		firstName:"test"
	});

	//delete many
	/*
		Syntax:
			db.collectionName.deleteMany({criteria});

	*/

	db.users.deleteMany({
		firstName:"Bill"
	});

	//Advanced Queries

	//Query an embedded document

	db.users.find({
		contact:{
			phone:"87654321",
			email:"stephenhawking@gmail.com"
		}
	})

	//query on nested field

	db.users.find(
		{"contact.email": "janedoe@gmail.com"}
	)

	//querying an array with exact elements

	db.users.find({courses: ["CSS","Javascript","Python"]})

	//querying an array w/o regard to order

	db.users.find({ courses:{ $all: ["React","Python"] } })


	//query an embedded array

	//added another document
	db.users.insert({

		namearr:[
			{
				namea:"juan"
			},
			{
				nameb:"tamad"
			}
		]

	})

	db.users.find({
		namearr:
		{
			namea:"juan"
		}
	})